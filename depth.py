def get_depth(data):
    result = {}
    depth = 1
    store = list()
    store.append(data)

    for obj in store:
        for key, value in obj.items():
            if type(value) is type({}):
                store.append(value)
            result.__setitem__(key, depth)
        depth += 1

    return result


def print_depth(data):
    result = get_depth(data)
    for key, value in result.items():
        print(key+" "+str(value))


if __name__ == '__main__':
    data = {
        'key1': 1,
        'key2': {
            'key3': 3,
            'key4': {
                'key5': 5
            }
        }
    }

    print_depth(data)

