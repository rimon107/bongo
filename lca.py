class Node:
    def __init__(self, value, parent=None):
        self.value = value
        if parent is None:
            self.parent = self
        else:
            self.parent = parent


def get_depth(node):
    depth = 0
    while node.value is not node.parent.value:
        node = node.parent
        depth += 1
    return depth


def lca(node1, node2):
    depth_1 = get_depth(node1)
    depth_2 = get_depth(node2)

    while depth_1 != depth_2:
        if depth_1 > depth_2:
            node1 = node1.parent
            depth_1 -= 1
        else:
            node2 = node2.parent
            depth_2 -= 1

    while node1.value != node2.value:
        if depth_1 == -1:
            return None
        depth_1 -= 1
        node1 = node1.parent
        node2 = node2.parent

    return node1


if __name__ == '__main__':
    node1 = Node(1, None)
    node2 = Node(2, node1)
    node3 = Node(3, node1)
    node4 = Node(4, node2)
    node5 = Node(5, node2)
    node6 = Node(6, node3)
    node7 = Node(7, node3)
    node8 = Node(8, node4)
    node9 = Node(9, node4)

    node10 = Node(8, None)
    node11 = Node(11, node10)

    first_node = node7
    second_node = node9

    result = lca(first_node, second_node)

    if result is None:
        print("nodes must be in same tree")
    else:
        print('lca of ', first_node.value ,' and ', second_node.value,' is ',  result.value)




